package com.twork.virtual.assist.airtravel;

import com.twork.virtual.assist.airtravel.beans.Event;
import java.lang.String;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

/**
 * A JAX-RS interface.  An implementation of this interface must be provided.
 */
@Path("/events")
public interface EventsResource {
  /**
   * Gets a list of all `Event` entities.
   */
  @GET
  @Produces("application/json")
  List<Event> getevents();

  /**
   * Creates a new instance of a `Event`.
   */
  @POST
  @Consumes("application/json")
  void createEvent(Event data);

  /**
   * Gets the details of a single instance of a `Event`.
   */
  @Path("/{eventId}")
  @GET
  @Produces("application/json")
  Event getEvent(@PathParam("eventId") String eventId);

  /**
   * Updates an existing `Event`.
   */
  @Path("/{eventId}")
  @PUT
  @Consumes("application/json")
  void updateEvent(@PathParam("eventId") String eventId, Event data);

  /**
   * Deletes an existing `Event`.
   */
  @Path("/{eventId}")
  @DELETE
  void deleteEvent(@PathParam("eventId") String eventId);
}
