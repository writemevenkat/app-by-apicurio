
package com.twork.virtual.assist.airtravel.beans;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;


/**
 * Root Type for Event
 * <p>
 * 
 * 
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "eventName",
    "eventSource",
    "eventTime"
})
public class Event {

    @JsonProperty("eventName")
    private String eventName;
    @JsonProperty("eventSource")
    private String eventSource;
    @JsonProperty("eventTime")
    private String eventTime;

    @JsonProperty("eventName")
    public String getEventName() {
        return eventName;
    }

    @JsonProperty("eventName")
    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    @JsonProperty("eventSource")
    public String getEventSource() {
        return eventSource;
    }

    @JsonProperty("eventSource")
    public void setEventSource(String eventSource) {
        this.eventSource = eventSource;
    }

    @JsonProperty("eventTime")
    public String getEventTime() {
        return eventTime;
    }

    @JsonProperty("eventTime")
    public void setEventTime(String eventTime) {
        this.eventTime = eventTime;
    }

}
